# Github User App

A Swift iOS app for searching Github User by. Aldo Lazuardi

Built using XCode 13.1 (Swift 5)

### How to run

1. Clone this repo
2. Open shell window and navigate to project folder
3. Run `pod install`
4. Open `GithubUser.xcworkspace` and run the project on selected device or simulator
