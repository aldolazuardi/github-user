//
//  LoadingCell.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 19/08/22.
//

import UIKit

class LoadingCell: UITableViewCell {
    @IBOutlet weak var loader: UIActivityIndicatorView!
}
