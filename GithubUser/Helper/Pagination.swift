//
//  Pagination.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 19/08/22.
//

import Foundation

public class Pagination {
    private var pageSize: Int = 20
    private var currentPage: Int = 0
    private var nextPage: Int = 0
    private(set) var isLoading: Bool = false

    public init(pageSize: Int = 20) {
        self.pageSize = pageSize
        self.reset()
    }

    public func newPage(numberOfRows: Int) {
        currentPage += 1
        nextPage = numberOfRows < pageSize ? currentPage : currentPage + 1
    }

    public func isLastPage() -> Bool {
        return nextPage == currentPage
    }

    public func getNextPage() -> Int {
        return nextPage
    }

    public func getPageSize() -> Int {
        return pageSize
    }
    
    public func setLoading(_ value: Bool) {
        isLoading = value
    }
    
    public func shouldLoadMore() -> Bool {
        return !isLastPage() && !isLoading
    }
    

    public func reset() {
        currentPage = 0
        nextPage = 1
    }
}
