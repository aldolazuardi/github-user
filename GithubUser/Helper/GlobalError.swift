//
//  GlobalError.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 18/08/22.
//

import Foundation
import UIKit

extension UIViewController {
    func didReceivedError(error: String) {
        let alert = UIAlertController(
            title: "Error",
            message: error,
            preferredStyle: .alert
        )
        alert.addAction(
            UIAlertAction(
                title: "OK",
                style: .default,
                handler: nil
            )
        )
        present(alert, animated: true, completion: nil)
    }
}
