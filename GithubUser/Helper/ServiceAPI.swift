//
//  ServiceAPI.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 18/08/22.
//

import Foundation
import Alamofire

public struct NetworkHandler {
    public typealias Failed = (_ error: String) -> Void
    public typealias Success = (_ result: Data) -> Void
    public typealias Completion = (() -> Void)?
}

public class ServiceAPI {
    static var instance = ServiceAPI()
    var request: DataRequest?
    
    public func searchUsers(
        param: UserListModel.Request,
        success:  @escaping (_ data: UserListModel.Response) -> Void,
        failed: @escaping NetworkHandler.Failed,
        completion: NetworkHandler.Completion = nil
    ) {
        cancelRequest()
        var parameters = [String : String]()
        parameters["q"] = param.query
        parameters["page"] = "\(param.page)"
        parameters["per_page"] = "\(param.perPage)"
        request = AF.request(
            "https://api.github.com/search/users",
            parameters: parameters
        ).validate()
            .responseDecodable(of: UserListModel.Response.self) { response in
                switch response.result {
                case .success(let data):
                    success(data)
                case .failure(let error):
                    guard !error.isExplicitlyCancelledError else { return }
                    switch error.responseCode {
                    case 403:
                        failed("Has reached the limit")
                    default:
                        failed(error.localizedDescription)
                    }
                }
                completion?()
            }
    }
    
    public func cancelRequest() {
        request?.cancel()
    }
}


