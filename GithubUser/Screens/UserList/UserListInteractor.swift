//
//  UserListInteractor.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 17/08/22.
//

import Foundation

protocol UserListInteractorOutput: AnyObject {
    func viewDidLoad()
    func didSearch(request: UserListModel.Request, completion: NetworkHandler.Completion)
    func didClearSearch()
    func didLoadMore(page: Int, completion: NetworkHandler.Completion)
}

class UserListInteractor: UserListInteractorOutput {
    var presenter: UserListPresenterInput?
    var userWorker: UserListSearchLogic?
    var request: UserListModel.Request?
    private var users: [User] = []
    
    func viewDidLoad() {}
    
    func didLoadMore(
        page: Int,
        completion: NetworkHandler.Completion
    ) {
        guard var request = request else { return }
        request.page = page
        userWorker?.searchUsers(
            param: request,
            success: { data in
                self.presenter?.didLoadMoreUsers(data)
            },
            failed: didFailed,
            completion: completion
        )
    }
    
    func didSearch(
        request: UserListModel.Request,
        completion: NetworkHandler.Completion
    ) {
        self.request = request
        userWorker?.searchUsers(
            param: request,
            success: { data in
                self.presenter?.didReceiveUsers(data)
            },
            failed: didFailed,
            completion: completion
        )
    }
    
    func didClearSearch() {
        ServiceAPI.instance.cancelRequest()
    }
    
    func didFailed(_ error: String) {
        presenter?.didFailed(error)
    }
}
