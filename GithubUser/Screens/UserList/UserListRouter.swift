//
//  UserListRouter.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 17/08/22.
//

import UIKit

protocol TitlesRouterLogic: AnyObject {
    var navigationController: UINavigationController? { get }
    func routeToDetail(with id: String)
}

class UserListRouter: TitlesRouterLogic {
    weak var navigationController: UINavigationController?
    
    func routeToDetail(with id: String) {
       // To Detail Page using `navigationController?.pushViewController`
    }
}
