//
//  UserListViewController.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 17/08/22.
//

import UIKit

protocol UserListPresenterOutput: AnyObject {
    func presenter(didRetrieveUsers users: [User], isLoadMore: Bool)
    func showError(_ errorMessage: String)
}

class UserListViewController: UIViewController {
    var interactor: UserListInteractor?
    var router: UserListRouter?
    var userListView: UserListView?
    private var users: [User] = []
    private var pagination: Pagination = Pagination()
    lazy var searchBar: UISearchBar = UISearchBar()
    enum TableSection: Int {
        case userList
        case loader
    }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        view = userListView
        userListView?.tableView.delegate = self
        userListView?.tableView.dataSource = self
        searchBar.delegate = self
        setUpSearchBar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.viewDidLoad()
    }
    
    private func setUpSearchBar() {
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.placeholder = "Search Github User"
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        navigationItem.titleView = searchBar
    }
    
    private func reset() {
        pagination = Pagination()
        users = []
        userListView?.reload()
    }
    
}

extension UserListViewController: UserListPresenterOutput {
    func presenter(didRetrieveUsers users: [User], isLoadMore: Bool) {
        if !isLoadMore {
            self.users = []
        }
        self.users.append(contentsOf: users)
        self.pagination.newPage(numberOfRows: users.count)
        self.pagination.setLoading(false)
        self.userListView?.reload()
    }
    
    func showError(_ errorMessage: String) {
        self.didReceivedError(error: errorMessage)
    }
}

extension UserListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        reset()
        guard !searchText.isEmpty else {
            interactor?.didClearSearch()
            return
        }
        let param = UserListModel.Request(
            query: searchText,
            page: pagination.getNextPage(),
            perPage: pagination.getPageSize()
        )
        self.pagination.setLoading(true)
        interactor?.didSearch(request: param, completion: nil)
    }
}

extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let listSection = TableSection(rawValue: section) else { return 0 }
        switch listSection {
        case .userList:
            self.users.isEmpty ?
            self.userListView?.showPlaceholder() :
            self.userListView?.hidePlaceholder()
            return self.users.count
        case .loader:
            return pagination.isLastPage() ? 0 : 1
        }
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = TableSection(rawValue: indexPath.section) else { return UITableViewCell() }
        switch section {
        case .userList:
            guard let cell = tableView
                    .dequeueReusableCell(withIdentifier: "Cell") as? UserCell else { return UITableViewCell() }
            cell.setupUser(users[indexPath.row])
            return cell
        case .loader:
            guard let cell = tableView
                    .dequeueReusableCell(withIdentifier: "LoadingCell") as? LoadingCell else { return UITableViewCell() }
            return cell
        }
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let section = TableSection(rawValue: indexPath.section), section == .userList else { return }
        let lastIndex = self.users.count - 1
        if indexPath.row == lastIndex, pagination.shouldLoadMore() {
            self.pagination.setLoading(true)
            self.interactor?.didLoadMore(page: pagination.getNextPage(), completion: nil)
        }
    }
}
