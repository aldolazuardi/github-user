//
//  UserListConfigurator.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 17/08/22.
//

import UIKit

class UserListConfigurator {
    static func configure(viewController: UserListViewController) {
        let view = UserListView()
        let router = UserListRouter()
        let interactor = UserListInteractor()
        let presenter = UserListPresenter()
        let userWorker = UserListWorker()
        viewController.userListView = view
        viewController.router = router
        viewController.interactor = interactor
        interactor.presenter = presenter
        interactor.userWorker = userWorker
        presenter.viewController = viewController
        router.navigationController = viewController.navigationController
    }
}

