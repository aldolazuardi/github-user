//
//  UserListView.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 17/08/22.
//

import UIKit

class UserListView: UIView {
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = 80
        tableView.register(
            UINib(nibName: "UserCell", bundle: nil),
            forCellReuseIdentifier: "Cell"
        )
        tableView.register(
            UINib(nibName: "LoadingCell", bundle: nil),
            forCellReuseIdentifier: "LoadingCell"
        )
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    lazy var emptyUserLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20)
        label.text = "User not found"
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        overrideUserInterfaceStyle = .light
        self.backgroundColor = .white
        self.addSubview(tableView)
        self.addSubview(emptyUserLabel)
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: self.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            tableView.topAnchor.constraint(equalTo: self.topAnchor),
        ])
        NSLayoutConstraint.activate([
            emptyUserLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            emptyUserLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
        ])
    }
    
    public func showPlaceholder() {
        UIView.animate(withDuration: 0.5) {
            self.emptyUserLabel.alpha = 1.0
            self.tableView.alpha = 0.0
        }
    }
    
    public func hidePlaceholder() {
        UIView.animate(withDuration: 0.5) {
            self.emptyUserLabel.alpha = 0.0
            self.tableView.alpha = 1.0
        }
    }
    
    public func reload() {
        self.tableView.reloadData()
    }
    
}
