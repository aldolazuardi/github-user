//
//  UserCell.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 17/08/22.
//

import UIKit
import SDWebImage

class UserCell: UITableViewCell {
    @IBOutlet weak var displayImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private func setupCell(image: String, name: String) {
        selectionStyle = .none
        titleLabel.text = name
        guard let url = URL(string: image) else { return }
        displayImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        displayImage.sd_setImage(with: url)
    }
    
}

extension UserCell {
    func setupUser(_ user: User) {
        setupCell(image: user.avatarUrl ?? "", name: user.login ?? "")
    }
}
