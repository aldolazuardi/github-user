//
//  UserListWorker.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 19/08/22.
//

import UIKit

protocol UserListSearchLogic {
    func searchUsers(
        param: UserListModel.Request,
        success:  @escaping (_ data: UserListModel.Response) -> Void,
        failed: @escaping NetworkHandler.Failed,
        completion: NetworkHandler.Completion
    )
}

class UserListWorker: UserListSearchLogic {
    func searchUsers(
        param: UserListModel.Request,
        success:  @escaping (_ data: UserListModel.Response) -> Void,
        failed: @escaping NetworkHandler.Failed,
        completion: NetworkHandler.Completion
    ) {
        ServiceAPI.instance.searchUsers(
            param: param,
            success: success,
            failed: failed,
            completion: completion
        )
    }
}
