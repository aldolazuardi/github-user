//
//  UserListModel.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 17/08/22.
//

import Foundation

public class UserListModel: Codable  {
    public struct Request {
        public var query : String
        public var page : Int
        public var perPage : Int = 20
    }
    
    public struct Response : Codable {
        public let incompleteResults : Bool?
        public let items : [User]?
        public let totalCount : Int?

        enum CodingKeys: String, CodingKey {
            case incompleteResults = "incomplete_results"
            case items
            case totalCount = "total_count"
        }
        
        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            incompleteResults = try values.decodeIfPresent(Bool.self, forKey: .incompleteResults)
            items = try values.decodeIfPresent([User].self, forKey: .items)
            totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        }
    }
}

public struct User : Codable {
    public let avatarUrl : String?
    public let id : Int?
    public let login : String?

    enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case id
        case login
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        avatarUrl = try values.decodeIfPresent(String.self, forKey: .avatarUrl)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        login = try values.decodeIfPresent(String.self, forKey: .login)
    }
}
