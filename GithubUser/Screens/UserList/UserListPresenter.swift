//
//  UserListPresenter.swift
//  GithubUser
//
//  Created by Aldo Lazuardi on 17/08/22.
//

import Foundation

protocol UserListPresenterInput: AnyObject {
    func didReceiveUsers(_ data: UserListModel.Response)
    func didLoadMoreUsers(_ data: UserListModel.Response)
    func didFailed(_ error: String)
}

class UserListPresenter: UserListPresenterInput {
    weak var viewController: UserListPresenterOutput?
    
    func didReceiveUsers(_ data: UserListModel.Response) {
        viewController?.presenter(
            didRetrieveUsers: data.items ?? [],
            isLoadMore: false
        )
    }
    
    func didLoadMoreUsers(_ data: UserListModel.Response) {
        viewController?.presenter(
            didRetrieveUsers: data.items ?? [],
            isLoadMore: true
        )
    }
    
    func didFailed(_ error: String) {
        viewController?.showError(error)
    }
}
