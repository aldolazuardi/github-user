//
//  UserListPresenterTests.swift
//  GithubUserTests
//
//  Created by Aldo Lazuardi on 19/08/22.
//

import XCTest
@testable import GithubUser

class UserListPresenterTests: XCTestCase {
    private var sut: UserListPresenter!
    private var vc: UserListPresenterOutputMock!
    
    override func setUp() {
        super.setUp()
        vc = UserListPresenterOutputMock()
        sut = UserListPresenter()
        sut.viewController = vc
    }
    
    override func tearDown() {
        sut = nil
        vc = nil
        
        super.tearDown()
    }
    
    func test_givenPresenter_whenReceiveUsers_thenFuncCalled() {
        let data = Data("""
        {
          "items" : [{
            "login" : "aldo",
            "avatar_url": "https://avatars.githubusercontent.com/u/224724?v=4"
          }]
        }
        """.utf8)
        sut.didReceiveUsers(try! JSONDecoder().decode(UserListModel.Response.self, from: data))
        XCTAssertTrue(vc.presenterDidRetrieveUsersCalled.0)
    }
    
    func test_givenPresenter_whenReceiveUsers_thenReceiveUserValues() {
        let data = Data("""
        {
          "items" : [{
            "login" : "aldo",
            "avatar_url": "https://avatars.githubusercontent.com/u/224724?v=4"
          }]
        }
        """.utf8)
        sut.didReceiveUsers(try! JSONDecoder().decode(UserListModel.Response.self, from: data))
        XCTAssertEqual(vc.presenterDidRetrieveUsersCalled.3, "aldo")
        XCTAssertEqual(vc.presenterDidRetrieveUsersCalled.4, "https://avatars.githubusercontent.com/u/224724?v=4")
    }

    func test_givenPresenter_whenDidFailed_thenShowErrorCalled() {
        sut.didFailed("Has reached the limit")
        XCTAssertTrue(vc.showErrorCalled.0)
    }
    
    func test_givenPresenter_whenDidFailed_thenShowErrorShowed() {
        let errorMessage = "Has reached the limit"
        sut.didFailed(errorMessage)
        XCTAssertEqual(vc.showErrorCalled.1, errorMessage)
    }
}
