//
//  UserListInteractorTests.swift
//  GithubUserTests
//
//  Created by Aldo Lazuardi on 19/08/22.
//

import XCTest
@testable import GithubUser

class UserListInteractorTests: XCTestCase {
    private var sut: UserListInteractor!
    private var worker: UserListWorkerMock!
    private var presenter: UserListPresenterInputMock!
    
    override func setUp() {
        super.setUp()
        sut = UserListInteractor()
        worker = UserListWorkerMock()
        presenter = UserListPresenterInputMock()
        sut.presenter = presenter
        sut.userWorker = worker
    }
    
    override func tearDown() {
        sut = nil
        worker = nil
        presenter = nil
        super.tearDown()
    }
    
    func test_givenInteractor_whenSearchUsers_thenWorkerCalled() {
        let request = UserListModel.Request(query: "Aldo", page: 1, perPage: 10)
        sut.didSearch(request: request, completion: nil)
        XCTAssertTrue(worker.isSearchUserCalled)
        XCTAssertEqual(presenter.didReceiveUsersCalled.1, 1)
        XCTAssertFalse(self.presenter.didFailedCalled.0)
    }
    
    func test_givenInteractor_whenSearchUsersEmpty_thenDidReceiveCalled() {
        worker.isForceEmpty = true
        let request = UserListModel.Request(query: "Aldo", page: 1, perPage: 10)
        sut.didSearch(request: request, completion: {})
        XCTAssertTrue(self.presenter.didReceiveUsersCalled.0)
        XCTAssertEqual(self.presenter.didReceiveUsersCalled.1, 0)
        XCTAssertFalse(self.presenter.didFailedCalled.0)
    }
    
    func test_givenInteractor_whenSearchUsersFailed_thenDidReceiveCalled() {
        worker.isForceError = true
        let request = UserListModel.Request(query: "Aldo", page: 1, perPage: 10)
        sut.didSearch(request: request, completion: nil)
        XCTAssertFalse(self.presenter.didReceiveUsersCalled.0)
        XCTAssertEqual(self.presenter.didReceiveUsersCalled.1, 0)
        XCTAssertEqual(self.presenter.didFailedCalled.1, "Error")
        
    }
    
    func test_givenInteractor_whenLoadMoreUsers_thenWorkerCalled() {
        sut.request = UserListModel.Request(query: "Aldo", page: 1, perPage: 10)
        sut.didLoadMore(page: 2, completion: nil)
        XCTAssertTrue(worker.isSearchUserCalled)
        XCTAssertEqual(presenter.didLoadMoreUsersCalled.1, 1)
        XCTAssertFalse(self.presenter.didFailedCalled.0)
    }
    
    func test_givenInteractor_whenLoadMoreUsersEmpty_thenDidReceiveCalled() {
        worker.isForceEmpty = true
        sut.request = UserListModel.Request(query: "Aldo", page: 1, perPage: 10)
        sut.didLoadMore(page: 2, completion: nil)
        XCTAssertTrue(self.presenter.didLoadMoreUsersCalled.0)
        XCTAssertEqual(self.presenter.didLoadMoreUsersCalled.1, 0)
        XCTAssertFalse(self.presenter.didFailedCalled.0)
    }
    
    func test_givenInteractor_whenLoadMoreUsersFailed_thenDidReceiveCalled() {
        worker.isForceError = true
        sut.request = UserListModel.Request(query: "Aldo", page: 1, perPage: 10)
        sut.didLoadMore(page: 2, completion: nil)
        XCTAssertFalse(self.presenter.didLoadMoreUsersCalled.0)
        XCTAssertEqual(self.presenter.didLoadMoreUsersCalled.1, 0)
        XCTAssertEqual(self.presenter.didFailedCalled.1, "Error")
        
    }
    
    func test_givenInteractor_whenDidLoadUsers_thenDidLoadMoreCalled() {
        sut.didLoadMore(page: 2, completion: {
            XCTAssertTrue(self.presenter.didReceiveUsersCalled.0)
        })
        
    }
    
    func test_givenInteractor_whenFaile_thenDidFailedCalled() {
        sut.didFailed("Error")
        XCTAssertTrue(presenter.didFailedCalled.0)
    }
    
}

private final class UserListWorkerMock: UserListSearchLogic {
    var isSearchUserCalled = false
    var isForceEmpty = false
    var isForceError = false
    
    func searchUsers(param: UserListModel.Request, success: @escaping (UserListModel.Response) -> Void, failed: @escaping NetworkHandler.Failed, completion: NetworkHandler.Completion) {
        if isForceEmpty {
            print("=== isForceEmpty")
            let data = Data("""
            {
              "items" : []
            }
            """.utf8)
            success(try! JSONDecoder().decode(UserListModel.Response.self, from: data))
        } else if isForceError {
            print("=== isForceError")
            failed("Error")
        } else {
            let data = Data("""
            {
              "items" : [{
            "login" : "aldo",
            "avatar_url": "https://avatars.githubusercontent.com/u/224724?v=4"
            }]
            }
            """.utf8)
            success(try! JSONDecoder().decode(UserListModel.Response.self, from: data))
        }
        isSearchUserCalled = true
    }
}

private final class UserListPresenterInputMock: UserListPresenterInput {
    var didReceiveUsersCalled: (Bool, Int) = (false, 0)
    func didReceiveUsers(_ data: UserListModel.Response) {
        didReceiveUsersCalled = (true, data.items?.count ?? 0)
    }
    
    var didLoadMoreUsersCalled: (Bool, Int) = (false, 0)
    func didLoadMoreUsers(_ data: UserListModel.Response) {
        didLoadMoreUsersCalled = (true, data.items?.count ?? 0)
    }
    
    var didFailedCalled: (Bool, String) = (false, "")
    func didFailed(_ error: String) {
        didFailedCalled = (true, error)
    }
    
}

