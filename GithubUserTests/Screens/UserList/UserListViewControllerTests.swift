//
//  UserListViewControllerTests.swift
//  GithubUserTests
//
//  Created by Aldo Lazuardi on 19/08/22.
//

import XCTest
@testable import GithubUser

class UserListViewControllerTests: XCTestCase {
    private var sut: UserListViewController!
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }

}

final class UserListPresenterOutputMock: UserListPresenterOutput {
    var presenterDidRetrieveUsersCalled: (Bool, Int, Bool, String, String)!
    func presenter(didRetrieveUsers users: [User], isLoadMore: Bool) {
        presenterDidRetrieveUsersCalled = (
            true,
            users.count,
            isLoadMore,
            users.first?.login ?? "",
            users.first?.avatarUrl ?? ""
        )
    }
    
    var showErrorCalled: (Bool, String)!
    func showError(_ errorMessage: String) {
        showErrorCalled = (true, errorMessage)
    }
}
